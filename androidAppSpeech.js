
$(document).ready(function () {

    //for android app only
function onDeviceReady() {
    console.log("Device is ready");

    //Speech Synthisis https://github.com/macdonst/TTS
function startupWin(voiceResult) {
    console.log("Startup win");
    // When result is equal to STARTED we are ready to play
    console.log("Result " + voiceResult);
    //TTS.STARTED==2 use this once so is answered
    if (voiceResult == 2) {
        navigator.tts.getLanguage(win, fail);

    }
}

$("#exitFromAppLink").click(function () {
    navigator.app.exitApp();
})




var myScroll;
var chartViewPageScroll;
function loaded() {
    
    chartViewPageScroll = new iScroll('visibleWidgetContainerDivControl');
    myScroll = new iScroll('wrapper');
}

function win(result) {
    console.log(result);
}

function fail(result) {
    console.log("Error = " + result);
}
loaded();

    //start the voice service
    navigator.tts.startup(startupWin, fail);


   



}

document.addEventListener("deviceready", onDeviceReady, true);

    //alert if internet connection dies 
document.addEventListener("offline", function () { showAlert("Attention", "You are offline. LogixVox needs a strong internet connection !", "Done"); }, false);





//for android app only

 


$("#SPEECH_BUTTON").click(function () {
    $("#widgetContainerDiv").css('display', 'none');
    
    var maxMatches = 1;
    var promptString = "Speak now"; 
    window.plugins.speechrecognizer.startRecognize(function(result) 
    {
        perpareForAnswer(result) // function to prepare the screen to recieve an answer to the question and show the chart
        getView(result, "voice")
    }, 
    function (errorMessage) 
    {
        console.log("Error message: " + errorMessage);
    }, maxMatches, promptString);

   
});



$("#QUESTION_AREA_BUTTON").on("click", function (e) {
    perpareForAnswer($("#QUESTION_AREA").val())
    getView($("#QUESTION_AREA").val())
})



$("#answerNavBar").hide();

//function to control the enter key on the keyboard
$(document).keypress(function (e) {
    if (e.which == 13) {

        if ($("#QUESTION_AREA").val() != ""){
            $("#QUESTION_AREA_BUTTON").click()
        }
    }
});

    //onclick of back arrow
$("i.fa.fa-arrow-left").click(function () {
    $("#AskAnotherQuestion").click()
})


$("i.fa.fa-star").click(function () {

    var currentColor = $('i.fa.fa-star').css("color");// get the current colour of the fav icon

    if (currentColor == 'rgb(255, 215, 0)')//if is gold
        {
            $('i.fa.fa-star').css("color", "rgb(179, 168, 168)")// change back to gray
        }
    else {

        saveQuestionToFavourates($("#bubbleYou")[0].innerText);
            $('i.fa.fa-star').css("color", "gold") // change to gold
        }
})


function saveQuestionToFavourates(question) {

    
    $("#landingPage").nimbleLoader("show");
    $.ajax({
        type: "Put",
        contentType: "application/json",
        dataType: "json",
        url: /*Base URL hidden for security reasons*/  + "/Favorite?question=" + question + "&systemcode=" + "CODA" + "&loginName=" + $("#UserName").val().toUpperCase(),
        success: function (msg) {
            $('#thelist').empty();//empty the div then
            getFavorites(false);
            $("#landingPage").nimbleLoader("hide");
            //showAlert("Attention", "You are logging in", "Done");

            //alert("added")
        },
        error: function (jqXHR, error, errorThrown) {
            showAlert("Error", error, "Done");
        }
    })

}


    // Show a custom alertDismissed
    //derived from https://github.com/katzer/cordova-plugin-local-notifications/
function showAlert(messageTitle, messageCore, messageButtonName) {
    navigator.notification.alert(
        messageCore,  // message
        alertDismissed,         // callback
        messageTitle,            // title
        messageButtonName                  // buttonName
    );
}






    //onclick of the footer navigator bar text view glyphicon
$("#answerNavBarTextView").click(function () {

    //toggle the color of the buttons to show what is highlighted
    $("#answerNavBarTextView").css('color', 'rgb(179, 168, 168)')
    $("#answerNavBarChartView").css('color', 'white')


    $(".btn-primary pull-right headerButton").css("display", "none");

    //show text bar and hide chart
    $("#widgetContainerDiv").css("display", "none")
    $("#widgetContainerHeaderDiv").css("display", "none")
    $("#widgetContainerHeaderDiv").children().hide();

    //show the text conversation
    showConversation()

});

    //onclick of the footer navigator bar chart view glyphicon
$("#answerNavBarChartView").click(function () {
    //toggle the colors to show what is highlighted
    $("#answerNavBarTextView").css('color', 'white')
    $("#answerNavBarChartView").css('color', 'rgb(179, 168, 168)')

    //hide the conversation div
    hideConversation()

    //hide text bar show chart
    $("#widgetContainerDiv").css("display", "block")
    $("#visibleWidgetContainerDivControl").css("display", "block")
    $("#widgetContainerHeaderDiv").css("display", "block")

    
    $(window).resize();
    
});


function showConversation() {
    $(".conversation").css("display", "block")
}

function hideConversation() {
    $(".conversation").css("display", "none")
}

    //when the user clicks the login button
$("#loginButton").click(function () {

    var username = $("#UserName").val().toString();
    var password = $("#Password").val().toString();
    userLogin(username, password);

    
});
//user Login function
function userLogin(userName, password) {
    $("#landingPage").nimbleLoader("show");

    $.ajax({
        url: /*Base URL hidden for security reasons*/ ,
        dataType: "json",
        data: {
           /*Login input data hidden for security reasons*/ 
        },
        type: "POST",
        success: function (msg) {
            //if success: move on to the home page
            if (msg.Success_Flag) {
                $("#landingPage").nimbleLoader("hide");
                $('#landingPage').css('display', 'none');
                $('body').css('background-color', '#FFFFFF');
                $('#homePage').css('display', 'block');
                getFavorites(false);
            }
            else {
                //if fail: try again
                $("#UserName").val('');
                $("#Password").val('');
                showAlert("Login Violation", "Invalid Username or Password entered", "Done");
                $("#landingPage").nimbleLoader("hide");
            }
        },
        error: function (jqXHR, error, errorThrown) {
        showAlert("Error", error, "Done");
    }
    });
}




$("#AskAnotherQuestion").click(function () {
 
    
    $('i.fa.fa-star').css("color", "rgb(179, 168, 168)")// change back to gray
    var bubbleMe = document.getElementById('bubbleMe');
    bubbleMe.textContent = "";
    var bubbleYou = document.getElementById('bubbleYou');
    bubbleYou.textContent = "";
    $("#QUESTION_AREA").val();

    $("#textArea").css("display","block");
    $("#widgetContainerDiv").css('display', 'none');
    $("#visibleWidgetContainerDivControl").css("display", "none");
    $('#answerNavBarTextView').trigger('click');
    $("#answerNavBar").hide();
    $("#questionNavBar").show();
    hideConversation()
    $("#backAndFavButtons").css("display", "none");
    
});







function getView(question, QuestionType) {
    $("#landingPage").nimbleLoader("show");

    $.getJSON(/*Base URL hidden for security reasons*/  + "?question=" + question, function (result) {
        if (result.SuccessFlag){
        $("#homePage").nimbleLoader("hide");
        //speak the question back to the use only if they have asked the question by voice
        if (QuestionType == "voice") {
            navigator.tts.speak(result.Answer);
        }

        //append the question to the answer box
        var transcription = document.getElementById('answerTranscription');
        transcription.textContent = result.Answer;

        var bubbleMe = document.getElementById('bubbleMe');
        bubbleMe.textContent = result.Answer;
        $("#QUESTION_AREA").val("");
        $("#textArea").css("display","none")

        
        BuildLayout("#widgetContainerDiv", result.ViewId);
        }
        else {
           
            showAlert("Woops", "I'm sorry, I didnt understand the question. Please try again", "Done");
            $("#homePage").nimbleLoader("hide");
            $("#AskAnotherQuestion").click()
        }

    }).done(function (e) {
       
        $("#landingPage").nimbleLoader("hide");

    })
     .fail(function () {
        
     })
}


function BuildLayout(layoutDiv, viewid) {
    var options = {
        viewID: viewid,
        userToken: /*user token hidden for security reasons*/,
        authenticationToken: 'Logix',
        //baseURL: 'http://172.21.0.120/LogixEco',
        baseURL: ''/*Base URL hidden for security reasons; sample base URL above*/,
        viewData: null,
        parameters: null,
        headerDivID: null
    };

    $(layoutDiv).data('VIEW_ID', viewid)
    $(layoutDiv).chartwidget(options);

    $(".glyphicon").css("font-size", "x - large");

}



    // function to prepare the screen to recieve an answer to the question and show the chart
function perpareForAnswer(result) {

    var transcription = document.getElementById('questionTranscription');


    
    $("#homePage").nimbleLoader("show");

    var bubbleYou = document.getElementById('bubbleYou');

    bubbleYou.textContent = result

    showConversation();//show the conversation class 
    $("#textArea").css("display", "none")
    $("#questionNavBar").hide();//toggle bottom nav bars
    $("#answerNavBar").show();
    $("#backAndFavButtons").css("display", "block");
}



$(window).on("resize", function () {

    if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
        $(selected).removeClass('slide-active');
    }


});

$("#target").click(function () {
    $('.navbar').offcanvas('toggle')
});


//function to get the favourates for the user that has loged in
function getFavorites(editingFlag) {
    $.getJSON(/*Base URL hidden for security reasons*/ "?systemcode=" + "CODA" + "&loginName=" + $("#UserName").val().toUpperCase(), function (result) {

        $.each(result, function (idx, resultItem) {
            var question = resultItem.QUESTION_TEXT ? resultItem.QUESTION_TEXT : resultItem.QUESTION_TEXT;
            var questionID = resultItem.QUESTION_ID ? resultItem.QUESTION_ID : resultItem.QUESTION_ID;
            $("#thelist").append("<li style='display: inline-table'><div style='padding-bottom: 5px;padding-top: 5px;'><div><i class='fa fa-search favs' style='font-size:xx-large; color:rgba(128, 128, 128, 0.84);float: right;'></i><i id='" + questionID + "' class='fa fa-minus-circle' style='display: none;color:red;float: right;font-size:xx-large; padding-right:2%' ></i></div>" + question + "</div></li>");
        });

        if (editingFlag) {
            $('#editFavorites').click();
            $('#editFavorites').click();
        }
    })
}


    //function to send favorited Question to the server
$("nav.cbp-spmenu.cbp-spmenu-vertical.cbp-spmenu-left").on('click', 'i.fa.fa-search', function (e) {
    $("#AskAnotherQuestion").click()
    $("#QUESTION_AREA").val(e.target.innerText.toString());
    $("#QUESTION_AREA_BUTTON").click();

});

$('#editFavorites').click(function () {

    if ($("#favoritesButton").html() == "Done") {
        $(".fa.fa-minus-circle").css("display", "none")
        $(".fa.fa-search.favs").css("display", "block")
        $("#favoritesButton").html('Edit');
        $('#favoritesButton').css("background-color", "#4F81BD")
    }
    else {
        $("#favoritesButton").html('Done');
        $('#favoritesButton').css("background-color", "red")
        $(".fa.fa-search.favs").css("display", "none")
        $(".fa.fa-minus-circle").css("display", "block")
    }

});

$("#favoritesPage").on('click', 'i.fa.fa-search.favs', function (e) {
    $("#homeLink").click();
    $("#QUESTION_AREA").val(e.currentTarget.parentNode.parentNode.innerText.toString());
    $("#QUESTION_AREA_BUTTON").click();
});

    //DELETE A favorite
$("#favoritesPage").on('click', 'i.fa.fa-minus-circle', function (e) {
    $("#landingPage").nimbleLoader("show");
    $.ajax({
        type: "Delete",
        contentType: "application/json",
        dataType: "json",
        url: /*Base URL hidden for security reasons*/ + "/?questionID=" + e.currentTarget.id + "&systemcode=" + "CODA" + "&loginName=" + $("#UserName").val().toUpperCase(),
        success: function (msg) {
            $('#thelist').empty();//empty the div then
            getFavorites(true);
            $("#landingPage").nimbleLoader("hide");
            
        },
        error: function (jqXHR, error, errorThrown) {
            showAlert("Error", error, "Done");
        }
    })
});


});