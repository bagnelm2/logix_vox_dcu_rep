import nltk
import json
import LogixQandA.settings

from restless.views import Endpoint

#convert lists to tuples
def tuplify(listything):
    if isinstance(listything, list): return tuple(map(tuplify, listything))
    if isinstance(listything, dict): return {k:tuplify(v) for k,v in listything.items()}
    return listything


class QandAAnswer(Endpoint):

    def QandA(self, question):
        # break up the sentence into words and tokens
        text = nltk.word_tokenize(question.upper())
        tokenised_question = nltk.pos_tag(text)

       #Code missing in this section for company confidentiality purposes 
	   #Code missing in this section for company confidentiality purposes
	   #Code missing in this section for company confidentiality purposes
	   #Code missing in this section for company confidentiality purposes
        
        if len(matching_results) > 0 :
            success = {'success_flag' : True} ;

            #just return view_id and result_data
            result = {k:v for (k,v) in dict(matching_results[0]).items() if k == 'view_id' or k == 'result_data'}

            result.update(success)

            return dict(result)
        else:
            failure = {'success_flag' : False, 'view_id' : 0, 'result_data' : ''} ;

            return dict(failure)

    def get(self, request):
        question = request.params.get('QUESTION', '')
        return self.QandA(question)

    
 

