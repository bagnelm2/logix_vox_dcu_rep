using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Reflection;
using System.Net.Http.Headers;
using System.Configuration;
using System.Text;
using LogixSpeechAPI.Models.Response;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Threading.Tasks;


namespace LogixSpeechAPI.Controllers.API
{
    [JsonArray]
    public class Answers { public List<Answer> JSON; }

    public class Answer
    {
        [JsonProperty("view_id")]
        public int view_id { get; set; }
        [JsonProperty("result_data")]
        public string result_data { get; set; }
        [JsonProperty("success_flag")]
        public Boolean success_flag { get; set; }
    }

    public class QuestionAnswerController : ApiController
    {

        public async Task<QuestionAnswerResponse> Get(string question, JObject parameterList)
        {
            QuestionAnswerResponse lr = new QuestionAnswerResponse();
            try
            {
                
                string URL = /*connection URL hidden for company confidentiality reasons*/ ;
                string urlParameters = "?QUESTION="+HttpUtility.UrlEncode(question);

                // get answer from server
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                Answer model = null;
                
                var task = await client.GetAsync(urlParameters);
                var jsonString = await task.Content.ReadAsStringAsync();
                model = JsonConvert.DeserializeObject<Answer>(jsonString);

                lr.Answer = model.result_data;
                lr.ViewId = model.view_id;
                lr.SuccessFlag = model.success_flag;
                lr.ErrorMessage = "";

            }
            catch (Exception ex)
            {
                lr.SuccessFlag = false;
                lr.ErrorMessage = ex.Message;
            }

            return lr;
            
        }


    }
}
