using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Reflection;
using System.Net.Http.Headers;
using System.Configuration;
using System.Text;
using LogixSpeechAPI.Models.Response;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace LogixSpeechAPI.Controllers.API
{
    public class FavoriteController : ApiController
    {
        /// <summary>
        /// method to get a users favourite question
        /// </summary>
        /// <param name="systemcode"></param>
        /// <param name="loginName"></param>
        /// <param name="parameterList"></param>
        /// <returns></returns>
        public List<FavouritesResponse> Get(string systemcode, string loginName, JObject parameterList)
        {
            List<FavouritesResponse> favouratesResponse = new List<FavouritesResponse>();
            
            try
            {
                //connect to server
                string connetionString = null;
                SqlConnection cnn ;
                connetionString = WebConfigurationManager.AppSettings["LGX_VOX_DB"];
                cnn = new SqlConnection(connetionString);
               
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("select * from TBL_FAVOURITE_QUESTIONS WHERE LOGIN_NAME ='" + loginName + "' AND SYSTEM_CODE = '" + systemcode + "'", cnn);
                    SqlDataReader dr;
                    dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        FavouritesResponse newItem = new FavouritesResponse();

                        newItem.SYSTEM_CODE = dr.GetString(0);
                        newItem.QUESTION_ID = dr.GetInt32(1);
                        newItem.LOGIN_NAME = dr.GetString(2);
                        newItem.QUESTION_TEXT = dr.GetString(3);


                        favouratesResponse.Add(newItem);
                    }
                    cnn.Close();
            }
            catch (Exception ex)
            {
                //implement logging
            }


            return favouratesResponse;
        }

        /// <summary>
        /// API method to delete a favourited question
        /// </summary>
        /// <param name="questionID">The ID of the Question</param>
        /// <param name="systemcode">The current systemcode</param>
        /// <param name="loginname">The Login name of the current user</param>
        /// <returns></returns>
        [System.Web.Mvc.HttpDelete]
        public APIResponseBase Delete(string questionID, string systemcode, string loginname)
        {
            APIResponseBase response = new APIResponseBase();

            try
            {
                //connect to server
                string connetionString = null;
                SqlConnection cnn;
                connetionString = WebConfigurationManager.AppSettings["LGX_VOX_DB"];
                cnn = new SqlConnection(connetionString);
                int intQuestionID = Int32.Parse(questionID);

                cnn.Open();
                SqlCommand cmd = new SqlCommand("delete from TBL_FAVOURITE_QUESTIONS where QUESTION_ID =" + intQuestionID 
                    + " and SYSTEM_CODE ='" + systemcode + "' and LOGIN_NAME ='" + loginname +"'", cnn);
                SqlDataReader dr;
                dr = cmd.ExecuteReader();
                response.SuccessFlag = true;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.SuccessFlag = false;
            }
                    
            return response;
        }


        /// <summary>
        /// Method to add a question to a users list of favourites
        /// </summary>
        /// <param name="question"></param>
        /// <param name="systemcode"></param>
        /// <param name="loginname"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPut]
        public APIResponseBase Put(string question, string systemcode, string loginname)
        {
            APIResponseBase response = new APIResponseBase();
            FavouritesResponse existingItem = new FavouritesResponse();

            try
            {
                //connect to server
                string connetionString = null;
                SqlConnection cnn;
                connetionString = WebConfigurationManager.AppSettings["LGX_VOX_DB"];
                cnn = new SqlConnection(connetionString);

                cnn.Open();

                //get the max id of the existing record

               
                SqlCommand getCmd = new SqlCommand( "SELECT * FROM TBL_FAVOURITE_QUESTIONS WHERE SYSTEM_CODE ='"+systemcode+"' AND LOGIN_NAME = '"+loginname+"' AND QUESTION_ID=(SELECT max(QUESTION_ID) FROM TBL_FAVOURITE_QUESTIONS)", cnn);
                SqlDataReader dr;
                dr = getCmd.ExecuteReader();

                //checks to see if the query has rows
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        existingItem.QUESTION_ID = dr.GetInt32(1);
                    }
                }//if not then the tbl is empty so set the id to 1
                else {
                    existingItem.QUESTION_ID = 0;
                }
                
                
                cnn.Close();

                //increase the ID
                 existingItem.QUESTION_ID++;

                //insert the record into the table
                SqlConnection seccondCnn;
                seccondCnn = new SqlConnection(connetionString);
                SqlDataReader secondDr;
                seccondCnn.Open();

                SqlCommand putCmd = new SqlCommand("INSERT INTO [TBL_FAVOURITE_QUESTIONS]VALUES ('" + systemcode + "'," + existingItem.QUESTION_ID + ",'" + loginname + "','" + question + "');", seccondCnn);
                secondDr = putCmd.ExecuteReader();

            }
            catch (Exception ex)
            {
                response.SuccessFlag = false;
                response.ErrorMessage = ex.Message;
            }

            response.SuccessFlag = true;
            return response;
        }


    }

  
}

