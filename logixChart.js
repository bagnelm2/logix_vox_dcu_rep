(function ($) {

    $.widget("lgx.chartwidget", $.lgx.basewidget, {
        options: {
        /*code missing from this section for company confidentiality and security reasons*/
        },

        _create: function () {
            var self = this, optList = self.options, el = self.element;

            // Call the base class
            this._super();

            optList.viewData = {};
            optList.viewData.headerDivID = "#widgetContainerHeaderDiv";
            optList.viewData.VIEW_ID = optList.viewID;
            optList.viewData.VIEW_NAME = "viewname";

            AddNavButtons(optList);

            addView(optList, $(this.widgetDiv).attr('id'), null, null);

        },
        _setOptions: function (newOpt) {
            newOpt.viewData = {};
            newOpt.viewData.headerDivID = "#widgetContainerHeaderDiv";
            newOpt.viewData.VIEW_ID = newOpt.viewID;
            newOpt.viewData.VIEW_NAME = "viewname";
            addView(newOpt, $(this.widgetDiv).attr('id'), null, null);
        },
        _destroy: function () {
            this.element.removeClass('#'+$(this.widgetDiv).attr('id'));
        }

    });
})(jQuery);

/**
 * Add Navigation Buttons to the chart
 * @constructor
 */
function AddNavButtons(optList) {
    if ($(optList.viewData.headerDivID).length > 0) {
        var houseIconPlusViewID = "houseIcon" + optList.viewData.VIEW_ID;
        var htmlForHouseIcon = "<div id='" + houseIconPlusViewID + "' class='pull-right headerButton'><span class='fa fa-home' style='font-size: 280%;color: rgb(79, 129, 189);'></span></div>";
        if ($("#" + houseIconPlusViewID).length == 0) {
            $(optList.viewData.headerDivID).append(htmlForHouseIcon);
        }
        else {
            $("#" + houseIconPlusViewID).show();
        }
        var backArrowIconPlusViewID = "backArrowIcon" + optList.viewData.VIEW_ID;
        var htmlForBackArrowIcon = "<div id='" + backArrowIconPlusViewID + "' class='pull-right headerButton'><span style='margin-left: 4px;font-size: 280%; color: rgb(79, 129, 189);' class='fa fa-arrow-circle-up'></span></div>";
        if ($("#" + backArrowIconPlusViewID).length == 0) {
            $(optList.viewData.headerDivID).append(htmlForBackArrowIcon);
        }
        else {
            $("#" + backArrowIconPlusViewID).show();
        }
    }
}

/**
 * Function to add a new view to the container
 * @constructor
 * @param {object} optList - options.
 * @param {string} chartDiv - Container ID 
 */
function addView(optList, chartDiv, clickList, seriesList) {

    $('#' + chartDiv).empty();

    $("#visibleWidgetContainerDivControl").nimbleLoader("show");


    //Need to format filter value list of a standard array and remove any of the Kendo MVVM properties.
    var filterList1 = [];
    var filterLocal = $.extend(false, [], optList.viewData.FilterList);
    if (filterLocal != undefined) {
        $.each(filterLocal, function (s, e) {
            e.FilterValues = JSON.parse(JSON.stringify(e.FilterValues));
            filterList1.push(e);
        });
    }

    //Need to format filter value list of a standard array and remove any of the Kendo MVVM properties.    
    var paramList1 = [];
    var paramLocal = $.extend(false, [], optList.viewData.ParameterList);
    if (paramLocal != undefined) {
        $.each(paramLocal, function (s, e) {
            if (e.itemList != undefined) {
                e.itemList = JSON.parse(JSON.stringify(e.itemList));
            }
            paramList1.push(e);
        });
    }

    //ajax hit to get data for chart
    $.ajax({
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        url: optList.baseURL + "/api/Chart",
        data: JSON.stringify({ /*Code missing for here for company confidentiality reasons*/ viewID: optList.viewData.VIEW_ID, parameterList: paramList1, clickList: clickList, filterList: filterList1, seriesList: seriesList }),
        headers: { "Authorization": optList.userToken },
        success: function (msg) {
            //ensure that the chart data is not empty
            if (msg != 'undefined' && msg.ChartData != "") {
                var myObject = eval('(' + msg.ChartData + ')');
                var oddFlag = true;

                //Home Button
                var $homeButton = $('[id*=homeButton' + optList.viewData.VIEW_ID + '_BImg]');

                $homeButton.data('chartData', myObject);
                $homeButton.data('viewID', optList.viewData.VIEW_ID);

                var houseIconPlusViewID = "houseIcon" + optList.viewData.VIEW_ID;
                $("#" + houseIconPlusViewID).data("chartData", myObject);
                $("#" + houseIconPlusViewID).data("viewID", optList.viewData.VIEW_ID);

                $("#" + houseIconPlusViewID).off('click');
                $("#" + houseIconPlusViewID).click(function (s, e) {

                    navigateBack(this, optList, chartDiv, clickList, "home");

                });

                //Back Button
                var $backButton = $('[id*=backButton' + optList.viewData.VIEW_ID + '_BImg]');

                $backButton.data('chartData', myObject);
                $backButton.data('viewID', optList.viewData.VIEW_ID);

                var backArrowIconPlusViewID = "backArrowIcon" + optList.viewData.VIEW_ID;
                $("#" + backArrowIconPlusViewID).data("chartData", myObject);
                $("#" + backArrowIconPlusViewID).data("viewID", optList.viewData.VIEW_ID);
                var houseIconExpandedPlusViewID = "houseIconExpanded" + optList.viewData.VIEW_ID;
                var backArrowIconExpandedPlusViewID = "backArrowIconExpanded" + optList.viewData.VIEW_ID;

                //show and hide the buttons based on clickList
                if (clickList != null) {


                    $('#' + houseIconExpandedPlusViewID).remove();
                    var htmlForHouseIcon = "<div id='" + houseIconExpandedPlusViewID + "' class='btn-primary pull-right headerButton'><span class='glyphicon glyphicon-home'></span></div>";
                    $('#expandedViewHeaderDiv').after(htmlForHouseIcon);

                    $('#' + backArrowIconExpandedPlusViewID).remove();
                    var htmlForBackArrowIcon = "<div id='" + backArrowIconExpandedPlusViewID + "' class='btn-primary pull-right headerButton'><span style='margin-left: 4px;' class='glyphicon glyphicon-arrow-left'></span></div>";
                    $('#expandedViewHeaderDiv').after(htmlForBackArrowIcon);

                    //Home Button drill down
                    $("#" + houseIconExpandedPlusViewID).data("chartData", myObject);
                    $("#" + houseIconExpandedPlusViewID).data("viewID", optList.viewData.VIEW_ID);

                    $("#" + houseIconExpandedPlusViewID).click(function (s, e) {

                        navigateBack(this, optList, chartDiv, clickList, "home");

                    });
                    //Back Button drilldown

                    $("#" + backArrowIconExpandedPlusViewID).data("chartData", myObject);
                    $("#" + backArrowIconExpandedPlusViewID).data("viewID", optList.viewData.VIEW_ID);

                    $("#" + backArrowIconExpandedPlusViewID).click(function (s, e) {

                        navigateBack(this, optList, chartDiv, clickList, "back");

                    });

                    //Home Button 
                    var $homeButton = $('[id*=homeButton' + optList.viewData.VIEW_ID + '_BImg]');

                    $homeButton.data('chartData', myObject);
                    $homeButton.data('viewID', optList.viewData.VIEW_ID);

                    var houseIconPlusViewID = "houseIcon" + optList.viewData.VIEW_ID;
                    $("#" + houseIconPlusViewID).data("chartData", myObject);
                    $("#" + houseIconPlusViewID).data("viewID", optList.viewData.VIEW_ID);

                    $("#" + houseIconPlusViewID).off('click');
                    $("#" + houseIconPlusViewID).click(function (s, e) {

                        navigateBack(this, optList, chartDiv, clickList, "home");

                    });

                    //Back Button drilldown
                    var $backButton = $('[id*=backButton' + optList.viewData.VIEW_ID + '_BImg]');

                    $backButton.data('chartData', myObject);
                    $backButton.data('viewID', optList.viewData.VIEW_ID);

                    var backArrowIconPlusViewID = "backArrowIcon" + optList.viewData.VIEW_ID;
                    $("#" + backArrowIconPlusViewID).data("chartData", myObject);
                    $("#" + backArrowIconPlusViewID).data("viewID", optList.viewData.VIEW_ID);


                    $("#" + houseIconPlusViewID).show();
                    $("#" + backArrowIconPlusViewID).show();
                }
                else {
                    $('#' + houseIconExpandedPlusViewID).remove();
                    $('#' + backArrowIconExpandedPlusViewID).remove();
                    $("#" + backArrowIconPlusViewID).hide();
                    $("#" + houseIconPlusViewID).hide();
                }

                $("#" + backArrowIconPlusViewID).off('click');
                $("#" + backArrowIconPlusViewID).click(function (s, e) {

                    navigateBack(this, optList, chartDiv, clickList, "back");

                });
                var seriesCounterForLegendFormat = -1 //as part of LGXECO-1069
                var options = {
                    chart: {
                        renderTo: chartDiv,
                        type: myObject.chartType,
                        options3d: {
                            enabled: myObject.Is3D,
                            alpha: 10,
                            beta: 15
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                formatter:
                                    function () {
                                        return '<b>' + this.point.name + '</b>: ' + Math.round(this.percentage) + ' %';
                                    }
                            },
                            events: {
                                click: selection
                            },
                            showInLegend: myObject.legend.Enabled
                        },
                        scatter: {
                            marker: {
                                radius: 5,
                                states: {
                                    hover: {
                                        enabled: true,
                                        lineColor: 'rgb(100,100,100)'
                                    }
                                }
                            },
                            states: {
                                hover: {
                                    marker: {
                                        enabled: false
                                    }
                                }
                            },
                            tooltip: {
                                headerFormat: '<b>{series.name}</b><br>',
                                pointFormat: '{point.x} cm, {point.y} kg'
                            }
                        },
                        series: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            stacking: myObject.Stacking,
                            events: {
                                click: selection,
                                checkboxClick: function () {
                                    chart.series[1].remove(true);

                                    jQuery.each(chart.series, function (idx, dataItem) {
                                        if (this.name != dataItem.name) {
                                            dataItem.checkboxClicked = false;
                                        }
                                    });
                                },
                                legendItemClick: function (event) { /// this function is called whenever a legend item is clicked
                                    // idea: try get the item clicked and remove/add it to a list, this list matches the series array
                                    var seriesIndex = this.index;
                                    var series = this.chart.series;

                                }
                            }
                        }
                    },
                    legend: {
                        enabled: myObject.legend.Enabled,
                        labelFormatter: function (a) {

                            return this.name;

                            // rolling back this for further discussion

                            //as part of LGXECO-1069
                            var total = 0;
                            for (var i = this.yData.length; i--;) { total += this.yData[i]; };
                            seriesCounterForLegendFormat++

                            try {
                                if (total.toFixed(options.series[seriesCounterForLegendFormat].data[0].noDecimals) != 'NaN')
                                    return this.name + ' ' + total.toFixed(options.series[seriesCounterForLegendFormat].data[0].noDecimals);
                                else
                                    return this.name;

                            }
                            catch (err) {
                                return this.name;
                            }

                        }
                    },
                    title: {
                        text: myObject.caption
                    },
                    tooltip:
                        {
                            enabled: true,
                            positioner: function (boxWidth, boxHeight, point) {
                                return { x: point.plotX + 20, y: point.plotY - 45 };
                            }
                        },
                    xAxis: {
                        categories: myObject.xaxis.Categories,
                        labels:
                            {
                                enabled: myObject.xaxis.LabelEnabled,
                                rotation: myObject.xaxis.rotated,
                                align: myObject.xaxis.LabelAlign,
                                formatter: function () {
                                    if (myObject.xaxis.StaggerLabels) {
                                        oddFlag = !oddFlag;
                                        return (oddFlag ? '<br/>' : '') + this.value;
                                    }
                                    else {
                                        return this.value;
                                    }
                                }
                            },
                        title: {
                            text: myObject.xaxis.Title
                        },
                        reversed: myObject.xaxis.reversed
                    },
                    yAxis: myObject.yaxis,
                    series: myObject.serList,
                    exporting: {
                        enabled: false
                    }
                };


                //if the chart is of type pie and there is no labels on the measures then don't display the arrows for the labels as per LGXECO-405
                if (options.chart.type == "pie") {
                    jQuery.each(options.series, function (i, seriesItem) {
                        jQuery.each(seriesItem.data, function (idx, dataItem) {
                            if (!dataItem.dataLabels.showNames && !dataItem.dataLabels.showPercentageInLabel && !dataItem.dataLabels.showvalues) {
                                dataItem.dataLabels.enabled = false;
                            }
                        })
                    })
                }

                for (var i = 0; i < options.yAxis.length; i++) {
                    options.yAxis[i].reversed = myObject.yaxis[i].Reversed;
                    options.yAxis[i].title = myObject.yaxis[i].Title;
                    if (myObject.yaxis[i].Title != null) {
                        if (i > 1) {
                            options.yAxis[i].title = null;
                        }
                        else {
                            options.yAxis[i].title.text = myObject.yaxis[i].Title.Text;
                        }
                    }
                    options.yAxis[i].opposite = myObject.yaxis[i].Opposite;
                    options.yAxis[i].reversedStacks = false;

                    options.yAxis[i].allowDecimals = false;
                }

                SetChartSpecificProperties(options, myObject);

                options.userToken = optList.userToken;
                options.parameters = optList.parameters;
                options.viewData = optList.viewData;
                options.authenticationToken = optList.authenticationToken;
                options.baseURL = optList.baseURL;

                var chart = new Highcharts.Chart(options);

                chart.rawData = myObject;

                //Trendline                    
                if (myObject.ShowTrendlines == true) {
                    ShowTrendline(chart, myObject, options.viewData.VIEW_ID);
                }
            }
            else {

                var errorHtml = "<div style='display:none'><p>Failed to Load Chart</p></div>";

                var colErrorDiv = $(errorHtml).appendTo($('#' + chartDiv));
                $(colErrorDiv).show();
            }

            //colDiv.hide();
            $("#visibleWidgetContainerDivControl").nimbleLoader("hide");
        }
    });
}


function selection(event, optList) {
    //panel-heading dashHeading


    if (event.point.url != null && event.point.url != '') {
        var chart = this;
        var activeSeries = [];
        var series = chart.chart.series;
        $.each(series, function (index, item) {
            if (item.visible) {
                activeSeries.push(item.name);
            }
        });
        event.point.options.chartContext.ActiveSeriesList = activeSeries;
        //var seriesIndex = this.index;
        //var series = this.chart.series;

        var serClicked = event.currentTarget.name;
        var catClicked = '';

        if (chart.options.type == 'pie') {
            catClicked = event.point.options.name;
        }
        else {
            catClicked = event.point.category;
        }

        //Escape invalid characters
        catClicked = SanitizeURLString(catClicked);

        if (event.point.options.chartContext.GroupNavigationList == null) {
            event.point.options.chartContext.GroupNavigationList = [];
        }

        if (event.point.options.chartContext.ActiveSeriesList == null) {
            event.point.options.chartContext.ActiveSeriesList = [];
        }

        var navList = [];

        for (var i in event.point.options.chartContext.GroupNavigationList) {
            navList.push(SanitizeURLString(event.point.options.chartContext.GroupNavigationList[i]));
        }

        var destURL = event.currentTarget.chart.options + "/DashboardDrilldown.html?" + event.point.url + catClicked + "&drillPath=" + event.point.options.chartContext.GroupNavigationList;

        navList.push(catClicked);

        var optList = event.currentTarget.chart.options;
        if (navList.length >= event.point.options.chartContext.DrilldownLevelCount) {
            ////if here, we've reached the lowest level of drilldown


        }
        else {
            //if there are still group levels to drilldown go to the next one
            var viewID = event.point.options.chartContext.ViewID;
            //set the FromGroup as -1 to indicate to use the top level grouping for the chart
            addView(event.currentTarget.chart.options, "logixWidget" + viewID, navList, activeSeries);
        }
    }






}




//Navigate up one level of the drilldown Hierarchy
function navigateBack(clickItem, optList, chartDiv, clickList, destination) {

    var chartData = $(clickItem).data('chartData');
    var viewID = $(clickItem).data('viewID');

    var activeSeries = [];
    var series = chartData.serList;
    $.each(series, function (index, item) {
        if (item.visible) {
            activeSeries.push(item.name);
        }
    });


    var catClicked = chartData.caption;

    catClicked = catClicked.substring(catClicked.lastIndexOf('('));

    catClicked = catClicked.replace('(', '');
    catClicked = catClicked.replace(')', '');

    var GroupNavigationList = catClicked.split(',');

    //Remove the last element to navigate one level up.
    GroupNavigationList.pop();

    if (GroupNavigationList.length == 0) {
        GroupNavigationList = null;
    }
    else {
        for (var i in GroupNavigationList) {
            GroupNavigationList[i] = SanitizeURLString(GroupNavigationList[i]);
        }
    }

    //Escape invalid characters
    catClicked = SanitizeURLString(catClicked);

    //GroupNavigationList.push(catClicked);

    //if there are still group levels to drilldown go to the next one
    var dpHeight = 850;
    var dpWidth = 1250;

    if (typeof (dockManager) != 'undefined') {
        var dp = dockManager.GetPanelByUID("dockPanel" + viewID);
        dpHeight = dp.height;
        dpWidth = dp.width;
    }

    //set the FromGroup as -1 to indicate to use the top level grouping for the chart
    if (destination == "back") {
        if (clickList.length != 0) {
            var length = clickList.length;
            length = length - 1;//because arrays start at 0 not 1
            //if there is at least 1 item in the array
            if (length < 1) {
                clickList = null;
            }
            else {
                var cclickList = clickList.pop();//kill off the last array value to allow for backspace
            }
        }
        else {
            clickList = null;
        }
    }
    else {
        clickList = null;
    }
    addView(optList, chartDiv, clickList, activeSeries);//recreate the view based on the new values
}


